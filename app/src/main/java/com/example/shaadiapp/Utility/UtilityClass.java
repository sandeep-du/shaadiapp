package com.example.shaadiapp.Utility;

import android.content.Context;
import android.net.ConnectivityManager;

import com.example.shaadiapp.Model.Result;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class UtilityClass {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static Type getType() {
        Type type = new TypeToken<ArrayList<Result>>() {
        }.getType();
        return type;

    }
}

package com.example.shaadiapp.Model;

import java.util.ArrayList;

public class Favourites {
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getNat() {
        return nat;
    }

    public void setNat(String nat) {
        this.nat = nat;
    }

    public ArrayList<Name> getNameList() {
        return nameList;
    }

    public void setNameList(ArrayList<Name> nameList) {
        this.nameList = nameList;
    }

    public ArrayList<UserLocation> getLocationList() {
        return locationList;
    }

    public void setLocationList(ArrayList<UserLocation> locationList) {
        this.locationList = locationList;
    }

    public ArrayList<LoginCreds> getLoginCredsArray() {
        return loginCredsArray;
    }

    public void setLoginCredsArray(ArrayList<LoginCreds> loginCredsArray) {
        this.loginCredsArray = loginCredsArray;
    }

    public ArrayList<Dob> getDobArrayList() {
        return dobArrayList;
    }

    public void setDobArrayList(ArrayList<Dob> dobArrayList) {
        this.dobArrayList = dobArrayList;
    }

    public ArrayList<Registered> getRegisteredArrayList() {
        return registeredArrayList;
    }

    public void setRegisteredArrayList(ArrayList<Registered> registeredArrayList) {
        this.registeredArrayList = registeredArrayList;
    }

    public ArrayList<Id> getIdArrayList() {
        return idArrayList;
    }

    public void setIdArrayList(ArrayList<Id> idArrayList) {
        this.idArrayList = idArrayList;
    }

    public ArrayList<Picture> getPictureArrayList() {
        return pictureArrayList;
    }

    public void setPictureArrayList(ArrayList<Picture> pictureArrayList) {
        this.pictureArrayList = pictureArrayList;
    }

    private String gender;
    private String email;
    private String phone;
    private String cell;
    private String nat;
    private ArrayList<Name> nameList;
    private ArrayList<UserLocation> locationList;
    private ArrayList<LoginCreds> loginCredsArray;
    private ArrayList<Dob> dobArrayList;
    private ArrayList<Registered> registeredArrayList;
    private ArrayList<Id> idArrayList;
    private ArrayList<Picture> pictureArrayList;


}

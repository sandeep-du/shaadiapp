package com.example.shaadiapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.shaadiapp.MainActivity.MainActivity;
import com.example.shaadiapp.MainActivity.MainPresenterImpl;
import com.example.shaadiapp.Model.Result;
import com.example.shaadiapp.R;

import java.util.Date;
import java.util.List;

public class AdapterClass extends RecyclerView.Adapter<AdapterClass.ViewHolder> {

    private List<Result> parkingList;
    private Context context;
    private MainPresenterImpl mainPresenter;

    public AdapterClass(Activity context, List<Result> list, MainPresenterImpl mainPresenter) {
        this.parkingList = list;
        this.context = context;
        this.mainPresenter = mainPresenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleradapterlayout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Result result = parkingList.get(position);
        holder.profileName.setText(result.getName().getFirst() + " " + result.getName().getLast() + " , " + result.getDob().getAge().toString());
        holder.profileGender.setText(firstLetterToCap(result.getGender()));
        holder.profileLocation.setText(result.getLocation().getCity() + " , " + result.getLocation().getCountry());
        holder.profilePhone.setText(result.getPhone());
        Glide.with(context).load(result.getPicture().getLarge()).into(holder.cardImage);
        if (parkingList.get(position).isFav()) {
            holder.tvAccept.setText(context.getApplicationContext().getResources().getText(R.string.button_accepted));
            holder.tvReject.setText(context.getApplicationContext().getResources().getText(R.string.button_reject));
        }
        if (parkingList.get(position).isRejected()) {
            holder.tvAccept.setText(context.getApplicationContext().getResources().getText(R.string.button_accept));
            holder.tvReject.setText(context.getApplicationContext().getResources().getText(R.string.button_rejected));
        }
        holder.tvAccept.setOnClickListener(v -> {
            if (!parkingList.get(position).isFav()) {
                long date = (new Date()).getTime();
                boolean isFavourite = mainPresenter.addFavs(date, parkingList.get(position), true, true);
                if (isFavourite) {
                    holder.tvAccept.setText(context.getApplicationContext().getResources().getText(R.string.button_accepted));
                    holder.tvReject.setText(context.getApplicationContext().getResources().getText(R.string.button_reject));
                    parkingList.get(position).setFav(true);
                    ((MainActivity) context).addToList(position, true);

                }
            } else {
                long date = (new Date()).getTime();
                boolean isRemovedFromFav = mainPresenter.addFavs(date, parkingList.get(position), false, true);
                if (isRemovedFromFav) {
                    holder.tvAccept.setText(context.getApplicationContext().getResources().getText(R.string.button_accept));
                    holder.tvReject.setText(context.getApplicationContext().getResources().getText(R.string.button_reject));
                    parkingList.get(position).setFav(false);
                    ((MainActivity) context).removeFromList(position);
                }
            }
        });

        holder.tvReject.setOnClickListener(v -> {
            if (parkingList.get(position).isFav()) {
                if (!parkingList.get(position).isRejected()) {
                    long date = (new Date()).getTime();
                    boolean isRejected = mainPresenter.addFavs(date, parkingList.get(position), true, false);
                    if (isRejected) {
                        holder.tvAccept.setText(context.getApplicationContext().getResources().getText(R.string.button_accept));
                        holder.tvReject.setText(context.getApplicationContext().getResources().getText(R.string.button_rejected));
                        parkingList.get(position).setRejected(true);
                        parkingList.get(position).setFav(false);
                        ((MainActivity) context).addToList(position, false);
                    }
                } else {
                    long date = (new Date()).getTime();
                    boolean isRemovedFromRejected = mainPresenter.addFavs(date, parkingList.get(position), false, false);
                    if (isRemovedFromRejected) {
                        holder.tvAccept.setText(context.getApplicationContext().getResources().getText(R.string.button_accept));
                        holder.tvReject.setText(context.getApplicationContext().getResources().getText(R.string.button_reject));
                        parkingList.get(position).setRejected(false);
                        ((MainActivity) context).removeFromList(position);
                    }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return parkingList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView profileName, profileGender, profileLocation, profilePhone, tvAccept, tvReject;
        private ImageView cardImage;


        private ViewHolder(View itemView) {
            super(itemView);
            cardImage = itemView.findViewById(R.id.cardImage);
            profileName = itemView.findViewById(R.id.profileName);
            profileGender = itemView.findViewById(R.id.profileGender);
            profileLocation = itemView.findViewById(R.id.profileLocation);
            profilePhone = itemView.findViewById(R.id.profilePhone);
            tvAccept = itemView.findViewById(R.id.tvAccept);
            tvReject = itemView.findViewById(R.id.tvReject);
        }

    }

    public String firstLetterToCap(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1);

    }
}

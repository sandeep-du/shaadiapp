package com.example.shaadiapp.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FavouritesDBHelper extends SQLiteOpenHelper {

    public FavouritesDBHelper(Context context) {
        super(context, DbSettings.DB_NAME, null, DbSettings.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " + DbSettings.DBEntry.TABLE + " ( " +
                DbSettings.DBEntry.COL_FAV_NAME + " TEXT NOT NULL, " +
                DbSettings.DBEntry.COL_FAV_AGE + " TEXT NOT NULL, " +
                DbSettings.DBEntry.COL_ISFAV + " TEXT NOT NULL, " +
                DbSettings.DBEntry.COL_ISREJECTED + " TEXT NOT NULL, " +
                DbSettings.DBEntry.COL_FAV_DATE + " TEXT NOT NULL, " +
                DbSettings.DBEntry.COL_FAV_GENDER + " TEXT NOT NULL , " +
                DbSettings.DBEntry.COL_FAV_LOCATION + " TEXT NOT NULL, " +
                DbSettings.DBEntry.COL_FAV_PHONE + " TEXT NOT NULL, " +
                DbSettings.DBEntry.COL_FAV_PROFILE_PICTURE + " TEXT NOT NULL);";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DbSettings.DBEntry.TABLE);
        onCreate(db);
    }

}

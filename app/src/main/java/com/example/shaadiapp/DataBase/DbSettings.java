package com.example.shaadiapp.DataBase;

import android.provider.BaseColumns;

public class DbSettings {
    public static final String DB_NAME = "shaadifavourites.db";
    public static final int DB_VERSION = 1;

    public class DBEntry implements BaseColumns {

        public static final String TABLE = "PROFILE";
        public static final String COL_ISFAV = "isfavourite";
        public static final String COL_ISREJECTED = "isRejected";
        public static final String COL_FAV_NAME = "name";
        public static final String COL_FAV_AGE = "age";
        public static final String COL_FAV_DATE = "date";
        public static final String COL_FAV_GENDER = "gender";
        public static final String COL_FAV_LOCATION = "location";
        public static final String COL_FAV_PHONE = "phone";
        public static final String COL_FAV_PROFILE_PICTURE = "profilepicture";


    }
}

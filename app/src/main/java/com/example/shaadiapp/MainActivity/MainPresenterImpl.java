package com.example.shaadiapp.MainActivity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.shaadiapp.DataBase.FavouritesDBHelper;
import com.example.shaadiapp.Model.ApiResponse;
import com.example.shaadiapp.Model.Dob;
import com.example.shaadiapp.Model.Favourites;
import com.example.shaadiapp.Model.Name;
import com.example.shaadiapp.Model.Picture;
import com.example.shaadiapp.Model.Result;
import com.example.shaadiapp.Model.UserLocation;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import static com.example.shaadiapp.DataBase.DbSettings.DBEntry.COL_FAV_AGE;
import static com.example.shaadiapp.DataBase.DbSettings.DBEntry.COL_FAV_DATE;
import static com.example.shaadiapp.DataBase.DbSettings.DBEntry.COL_FAV_GENDER;
import static com.example.shaadiapp.DataBase.DbSettings.DBEntry.COL_FAV_LOCATION;
import static com.example.shaadiapp.DataBase.DbSettings.DBEntry.COL_FAV_NAME;
import static com.example.shaadiapp.DataBase.DbSettings.DBEntry.COL_FAV_PHONE;
import static com.example.shaadiapp.DataBase.DbSettings.DBEntry.COL_FAV_PROFILE_PICTURE;
import static com.example.shaadiapp.DataBase.DbSettings.DBEntry.COL_ISFAV;
import static com.example.shaadiapp.DataBase.DbSettings.DBEntry.COL_ISREJECTED;
import static com.example.shaadiapp.DataBase.DbSettings.DBEntry.TABLE;

public class MainPresenterImpl implements MainPresenter {
    private MainView mainView;
    private ArrayList<Favourites> arrayList;
    String url = "https://randomuser.me/api/?results=10";
    Context context;
    ApiResponse apiResponse;
    FavouritesDBHelper mFavHelper;

    public MainPresenterImpl(MainView mainView, Activity context) {
        mFavHelper = new FavouritesDBHelper(context);
        this.mainView = mainView;
        this.context = context;
    }

    @Override
    public void getData() {
        mainView.showProgress();
        callService();

    }

    @Override
    public boolean addFavs(long date, Result result, boolean isAdded, boolean isFav) {
        float rowId = 0;
        SQLiteDatabase db = null;
        String whereClause = COL_FAV_NAME + "=?";
        String[] whereArgs = {result.getName().getFirst() + "-" + result.getName().getLast()};
        try {
            db = mFavHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COL_FAV_NAME, result.getName().getFirst() + "-" + result.getName().getLast());
            values.put(COL_FAV_AGE, result.getDob().getAge().toString());
            values.put(COL_FAV_DATE, date);
            values.put(COL_FAV_GENDER, result.getGender());
            values.put(COL_FAV_LOCATION, result.getLocation().getCity() + "-" + result.getLocation().getCountry());
            values.put(COL_FAV_PHONE, result.getPhone());
            if (isAdded) {
                if (isFav) {
                    values.put(COL_ISFAV, "Y");
                    values.put(COL_ISREJECTED, "N");

                } else {
                    values.put(COL_ISFAV, "N");
                    values.put(COL_ISREJECTED, "Y");
                }
            } else {
                values.put(COL_ISFAV, "N");
                values.put(COL_ISREJECTED, "N");
            }
            values.put(COL_FAV_PROFILE_PICTURE, result.getPicture().getLarge());
            rowId = db.update(TABLE,
                    values, whereClause, whereArgs);
            if (rowId <= 0) {
                rowId = db.insert(TABLE, null, values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (db != null)
                db.close();
        }


        if (rowId > 0)
            return true;
        else
            return false;
    }


    @Override
    public List<Result> showFavs() {
        List<Result> profileResults = new ArrayList<>();
        String whereClause = COL_ISFAV + "=?";
        String[] whereArgs = {"Y"};
        try {
            SQLiteDatabase db = mFavHelper.getWritableDatabase();
            Cursor cursor = db.query(TABLE, null, whereClause, whereArgs, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    Result result = new Result();
                    result.setGender(cursor.getString(cursor.getColumnIndex(COL_FAV_GENDER)));
                    result.setPhone(cursor.getString(cursor.getColumnIndex(COL_FAV_PHONE)));
                    result.setFav(true);
                    result.setRejected(false);
                    String userName = cursor.getString(cursor.getColumnIndex(COL_FAV_NAME));
                    String[] nameArray = userName.split("-");
                    Name name = new Name();
                    name.setFirst(nameArray[0]);
                    name.setLast(nameArray[1]);
                    result.setName(name);
                    Dob dob = new Dob();
                    dob.setAge(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_FAV_AGE))));
                    result.setDob(dob);
                    UserLocation userLocation = new UserLocation();
                    String location = cursor.getString(cursor.getColumnIndex(COL_FAV_LOCATION));
                    String[] locationArray = location.split("-");
                    userLocation.setCity(locationArray[0]);
                    userLocation.setCountry(locationArray[1]);
                    result.setLocation(userLocation);
                    Picture picture = new Picture();
                    picture.setLarge(cursor.getString(cursor.getColumnIndex(COL_FAV_PROFILE_PICTURE)));
                    result.setPicture(picture);
                    profileResults.add(result);

                }
                while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return profileResults;
    }


    private void callService() {
        try {
            RequestQueue queue = Volley.newRequestQueue(context);
            JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                    response -> {
                        try {
                            apiResponse = new Gson().fromJson(response.toString(),
                                    ApiResponse.class);
                            ((MainActivity) context).getResponse(apiResponse);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            mainView.hideProgress();
                        }

                    },
                    error -> Log.d("Error.Response", error.toString())
            );

            queue.add(getRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

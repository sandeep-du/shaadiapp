package com.example.shaadiapp.MainActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.example.shaadiapp.Adapter.AdapterClass;
import com.example.shaadiapp.Model.ApiResponse;
import com.example.shaadiapp.Model.Result;
import com.example.shaadiapp.R;
import com.example.shaadiapp.UriParser.UriDeserializer;
import com.example.shaadiapp.UriParser.UriSerializer;
import com.example.shaadiapp.Utility.UtilityClass;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainView {
    private MainPresenterImpl mainPresenter;
    List<Result> profileList = new ArrayList<>();
    AdapterClass adapterClass;
    RecyclerView recyclerView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progressBar);
        mainPresenter = new MainPresenterImpl(this, this);
        if (savedInstanceState == null) {
            if (UtilityClass.isNetworkAvailable(this))
                mainPresenter.getData();
            else {
                profileList = mainPresenter.showFavs();
                if (profileList.size() == 0)
                    Toast.makeText(this, getApplicationContext().getResources().getString(R.string.add_favs_offline), Toast.LENGTH_SHORT).show();
                else
                    checkAndSetAdapter();
            }
        }

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            String json = savedInstanceState.getString(getApplicationContext().getResources().getString(R.string.uri_class));
            if (!json.isEmpty()) {
                Gson gson = new GsonBuilder()
                        .registerTypeAdapter(Uri.class, new UriDeserializer())
                        .create();
                profileList = gson.fromJson(json, UtilityClass.getType());
                checkAndSetAdapter();
                (recyclerView.getLayoutManager()).scrollToPosition(savedInstanceState.getInt(getApplicationContext().getResources().getString(R.string.scrolled_recycler_osition)));


            }
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Uri.class, new UriSerializer())
                .create();
        outState.putString(getApplicationContext().getResources().getString(R.string.uri_class), gson.toJson(profileList));
        outState.putInt(getApplicationContext().getResources().getString(R.string.scrolled_recycler_osition), ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition());
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);

    }

    private void checkAndSetAdapter() {
        if (adapterClass == null) {
            adapterClass = new AdapterClass(this, profileList, mainPresenter);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            SnapHelper snapHelper = new PagerSnapHelper();
            recyclerView.setLayoutManager(mLayoutManager);
            snapHelper.attachToRecyclerView(recyclerView);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapterClass);
        } else
            adapterClass.notifyDataSetChanged();
    }

    public void getResponse(ApiResponse apiResponse) {
        profileList = apiResponse.getResults();
        checkAndSetAdapter();

    }

    public void addToList(int position, boolean isFav) {
        if (isFav) {
            profileList.get(position).setFav(true);
            profileList.get(position).setRejected(false);

        } else {
            profileList.get(position).setRejected(true);
            profileList.get(position).setFav(false);
        }
    }

    public void removeFromList(int position) {
        profileList.get(position).setFav(false);
        profileList.get(position).setRejected(false);
    }
}

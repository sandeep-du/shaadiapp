package com.example.shaadiapp.MainActivity;

import com.example.shaadiapp.Model.Result;

import java.util.List;

public interface MainPresenter {
    void getData();

    boolean addFavs(long date, Result result, boolean isAdded, boolean isFav);


    List<Result> showFavs();
}
